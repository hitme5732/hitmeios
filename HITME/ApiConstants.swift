//
//  ApiLinks.swift
//  RAYT
//
//  Created by SAI KUMAR on 09/09/21.
//

import Foundation

struct ApiNames {
   // static var baseURL:String = ""
    static var liveURL:String = "https://hitme.co.in/"
    static let token = liveURL + "oauth/token"
    static let registration = liveURL + "api/userregistration"
    static let login = liveURL + "api/checkuserlogin"
    static let updateProfile = liveURL + "api/updateprofile"
    static let changePassword = liveURL + "api/changepassword"
}
