//
//  PageControllerVC.swift
//  RAYT
//
//  Created by SAIKUMAR on 16/06/21.
//

import UIKit

class PageControllerVC: UIViewController {
    
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var skipBtn: UIButton!
    @IBOutlet weak var pageControler: UIPageControl!
    
    @IBOutlet weak var `pageCollectionView`: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        
        
        
    }
    
    
    @IBAction func getStartedAction(_ sender: Any) {
        let screenRef = self.storyboard?.instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
        self.navigationController?.pushViewController(screenRef, animated: true)
    }
    
    
    
//    func moveCollectionToFrame(contentOffset : CGFloat) {
//
//        let frame: CGRect = CGRect(x : contentOffset ,y : self.pageCollectionView.contentOffset.y ,width : self.pageCollectionView.frame.width,height : self.pageCollectionView.frame.height)
//        self.pageCollectionView.scrollRectToVisible(frame, animated: true)
//    }
}
extension PageControllerVC:UICollectionViewDelegate,UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PageControllerCVC", for: indexPath) as? PageControllerCVC else {
            fatalError("Unexpected indexPath")
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        let offSet = scrollView.contentOffset.x
//        let width = scrollView.frame.width
//        let horizontalCenter = width / 2
//        pageControler.currentPage = Int(offSet + horizontalCenter) / Int(width)
//    }
}
extension PageControllerVC:UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let picDimension = (collectionView.frame.width)
        return CGSize(width:(picDimension), height: collectionView.frame.height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

