//
//  PageControllerCVC.swift
//  RAYT
//
//  Created by SAIKUMAR on 16/06/21.
//

import UIKit

class PageControllerCVC: UICollectionViewCell {
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var contentLbl: UILabel!
    @IBOutlet weak var imgView2: UIImageView!
    @IBOutlet weak var imgView1: UIImageView!
    
}
