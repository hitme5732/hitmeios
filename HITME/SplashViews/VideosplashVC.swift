//
//  VideosplashVC.swift
//  HITME
//
//  Created by Naresh Oruganti on 28/09/21.
//

import UIKit
import SwiftGifOrigin

class VideosplashVC: UIViewController {

    @IBOutlet weak var playGifImg: UIImageView!
   
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.navigationController?.navigationBar.isHidden = true
        self.playGifImg.image = UIImage.gif(name: "Hitme fianl logo")
        // Do any additional setup after loading the view.
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            let valueSplash:Bool = (UserDefaults.standard.value(forKey: "VideoSplash") != nil)
            if valueSplash {
                let screenRef = self.storyboard?.instantiateViewController(withIdentifier: "PageControllerVC") as! PageControllerVC
                self.navigationController?.pushViewController(screenRef, animated: false)
            }else {
                let screenRef = self.storyboard?.instantiateViewController(withIdentifier: "SplashVC") as! SplashVC
                self.navigationController?.pushViewController(screenRef, animated: true)
            }
   
        }
    }

}
