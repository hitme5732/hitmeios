//
//  SplashVC.swift
//  HITME
//
//  Created by Naresh Oruganti on 27/09/21.
//

import UIKit
import SwiftGifOrigin

class SplashVC: UIViewController {

    @IBOutlet weak var mageLbl: UILabel!
    @IBOutlet weak var madeImage: UIImageView!
    @IBOutlet weak var logoImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        UserDefaults.standard.setValue(true, forKey: "VideoSplash")
        self.navigationController?.navigationBar.isHidden = true
        self.logoImage.image = UIImage.gif(name: "welcome")
        DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
            let screenRef = self.storyboard?.instantiateViewController(withIdentifier: "PageControllerVC") as! PageControllerVC
            self.navigationController?.pushViewController(screenRef, animated: false)
        }
        // Do any additional setup after loading the view.
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }

}
