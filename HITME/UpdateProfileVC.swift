//
//  UpdateProfileVC.swift
//  HITME
//
//  Created by SAI KUMAR on 08/10/21.
//

import UIKit

class UpdateProfileVC: UIViewController {

    @IBOutlet weak var saveProfileBtn: UIButton!
    @IBOutlet weak var termsBtn: UIButton!
    @IBOutlet weak var profilePicImg: UIImageView!
    @IBOutlet weak var firstNameTf: UITextField!
    @IBOutlet weak var profileNameLbl: UILabel!
    @IBOutlet weak var lastNameTf: UITextField!
    @IBOutlet weak var mobileNumLbl: UILabel!
    @IBOutlet weak var maleImgRef: UIImageView!
    @IBOutlet weak var femaleImgRef: UIImageView!
    @IBOutlet weak var othersImgRef: UIImageView!
    @IBOutlet weak var dobLbl: UILabel!
    var genderType = ""
    var imagePicker = UIImagePickerController()

    override func viewDidLoad() {
        super.viewDidLoad()
        saveProfileBtn.backgroundColor = .lightText
        saveProfileBtn.isUserInteractionEnabled = false

        self.imagePicker.delegate = self
        self.imagePicker.allowsEditing = true
        

    }
    @IBAction func backBtnTapped(_ sender: UIButton) {
    }
    @IBAction func skipBtnTapped(_ sender: UIButton) {
    }
    @IBAction func saveProfileBtnTapped(_ sender: UIButton) {
        
    }
    @IBAction func termsBtnTapped(_ sender: UIButton) {
        if self.termsBtn.tag == 1 {
            self.termsBtn.setImage(UIImage.init(named: "on"), for: UIControl.State.normal)
            self.termsBtn.tag = 2
            self.termsBtn.isSelected = true
            saveProfileBtn.isUserInteractionEnabled = false
            saveProfileBtn.backgroundColor = .black

        }
        else{
            self.termsBtn.setImage(UIImage.init(named: "off"), for: UIControl.State.normal)
            self.termsBtn.tag = 1
            saveProfileBtn.backgroundColor = .lightText
            saveProfileBtn.isUserInteractionEnabled = false
            self.termsBtn.isSelected = false
        }
        
    }
    @IBAction func profileBtnTapped(_ sender: UIButton) {
        
        let alert = UIAlertController(title: "Select an action\nFrom", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { (galleryBtn) in
            self.openGallery()
        }))
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (cameraBtn) in
            self.openCamera()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (cancelBtn) in
            
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera() {
        self.imagePicker.sourceType = .camera
        self.present(self.imagePicker, animated: true, completion: nil)
    }
    
    func openGallery() {
        self.imagePicker.sourceType = .photoLibrary
        self.present(self.imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func genderBtnTapped(_ sender: UIButton) {
        
        if sender.tag == 1 {
            maleImgRef.image = UIImage(named: "radioOn")
            femaleImgRef.image = UIImage(named: "radio")
            othersImgRef.image = UIImage(named: "radio")
            genderType = "Male"
        }else if sender.tag == 2{
            femaleImgRef.image = UIImage(named: "radioOn")
            maleImgRef.image = UIImage(named: "radio")
            othersImgRef.image = UIImage(named: "radio")
            genderType = "FeMale"

        }else if sender.tag == 3{
            othersImgRef.image = UIImage(named: "radioOn")
            femaleImgRef.image = UIImage(named: "radio")
            maleImgRef.image = UIImage(named: "radio")
            genderType = "Others"

        }
    }
    @IBAction func dobBtnTapped(_ sender: UIButton) {
        
    }
    @IBAction func contactAdminBtnTapped(_ sender: UIButton) {
        
    }
    
   
}
extension UpdateProfileVC:UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let editedImage = info[.editedImage] as? UIImage {
            self.profilePicImg.image = editedImage
        }else if let originalImage = info[.originalImage] as? UIImage {
            self.profilePicImg.image = originalImage
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}
