//
//  Service.swift
//  RAYT
//
//  Created by SAIKUMAR on 16/06/21.
//
import Foundation
import Alamofire
import UIKit
import SystemConfiguration

class  ServicesHelper : NSObject {
    
    static let sharedInstance = ServicesHelper()
    let headersw = [
           "Authorization": "key=a659508e607e9d3117cfc0ff168a741b",
       ]
    var errMessage: String!
      let imageV = UIImageView()
      let indicator: UIActivityIndicatorView = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
      
      func loader(view: UIView) -> () {
          indicator.frame = CGRect(x: 0,y: 0,width: 75,height: 75)
          indicator.layer.cornerRadius = 8
          indicator.center = view.center
        indicator.color = #colorLiteral(red: 0.9023000002, green: 0.2495641708, blue: 0.1642830968, alpha: 1)
     //   view.backgroundColor = UIColor.darkGray.withAlphaComponent(0.3)
          view.addSubview(indicator)
        imageV.image = UIImage(named: "Group 431")
          indicator.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
          indicator.bringSubviewToFront(view)
          UIApplication.shared.isNetworkActivityIndicatorVisible = true
          indicator.startAnimating()
          UIApplication.shared.beginIgnoringInteractionEvents()
      }
      
      func dissMissLoader()  {
          indicator.stopAnimating()
          imageV.removeFromSuperview()
          UIApplication.shared.endIgnoringInteractionEvents()
          UIApplication.shared.isNetworkActivityIndicatorVisible = false
      }
      
      func setCircleForImage(_ imageView : UIImageView){
          imageView.layer.cornerRadius = imageView.frame.size.width/2
          imageView.clipsToBounds = true
      }
    
  //  let manager = Alamofire.SessionManager.default
   // func manager.session.configuration.timeoutIntervalForRequest = 120
    //MARK:- GET SERVICE CALL
    func getDataServiceCall(url:String,postDict:[String:AnyObject], completionHandler : @escaping (Dictionary<String, Any>, NSError?) -> ()){
        Alamofire.request(url, method: .get, parameters: postDict).responseJSON { response in
            print("url ",url)
            print(response)
            switch response.result {
            case .success:
                // handle success here
                completionHandler(response.result.value as! Dictionary<String, Any>, response.result.error as NSError?)
                
                break
            case .failure:
                let dict:[String:Any] = ["error":"Server Error"]
                completionHandler(dict, response.result.error as NSError?)
                break
                
            
            }
            
            #if DEBUG
            
            
            #endif
        }
    }
    
    func getDataServiceCall23(url:String,postDict:[String:AnyObject], completionHandler : @escaping (Dictionary<String, Any>, NSError?) -> ()){
        Alamofire.request(url, method: .get, parameters: postDict).responseString { response in
            print("url ",url)
            print(response)
            switch response.result {
            case .success:
                // handle success here
                completionHandler(response.result.value as! Dictionary<String, Any>, response.result.error as NSError?)
                break
            case .failure:
                let dict:[String:Any] = ["error":"Server Error"]
                completionHandler(dict, response.result.error as NSError?)
                break
            
            }
            
            #if DEBUG
            
            
            #endif
        }
    }
    
    //MARK:- POST SERVICE
    func postServiceCall(url:String,PostDetails: [String:AnyObject], completionHandler : @escaping (Dictionary<String, Any>, NSError?) -> ()){
        print("loginu",url)
        print("logind",PostDetails)
        
        Alamofire.request(url, method: .post, parameters: PostDetails, encoding: URLEncoding.default, headers: headersw).responseJSON { response in
            print(response)
            
            switch response.result {
            case .success:
                // handle success here
                completionHandler(response.result.value as! Dictionary<String, Any>, response.result.error as NSError?)
                
            case .failure:
                let dict:[String:Any] = ["error":"Server Error"]
                completionHandler(dict, response.result.error as NSError?)
                break
            
            }
            #if DEBUG
            
            
            #endif
        }
       
    }
}





public class Reachability {
    
    class func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return false
        }
        
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        
        return (isReachable && !needsConnection)
        
    }
    
}
extension UIView {
    func showBlurLoader() {
        let blurLoader = BlurLoader(frame: frame)
        self.addSubview(blurLoader)
    }
    func removeBluerLoader() {
        if let blurLoader = subviews.first(where: { $0 is BlurLoader }) {
            blurLoader.removeFromSuperview()
        }
    }
}
class BlurLoader: UIView {
    var blurEffectView: UIVisualEffectView?
    override init(frame: CGRect) {
        let blurEffect = UIBlurEffect(style: .extraLight)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = frame
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.blurEffectView = blurEffectView
        super.init(frame: frame)
        addSubview(blurEffectView)
        addLoader()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    private func addLoader() {
        guard let blurEffectView = blurEffectView else { return }
        let activityIndicator = UIActivityIndicatorView(style: .large)
        activityIndicator.frame = CGRect(x: 0,y: 0,width: 75,height: 75)
        activityIndicator.layer.cornerRadius = 8
        activityIndicator.color = .white
        activityIndicator.backgroundColor = #colorLiteral(red: 0.9023000002, green: 0.2495641708, blue: 0.1642830968, alpha: 1)
        blurEffectView.contentView.addSubview(activityIndicator)
        activityIndicator.center = blurEffectView.contentView.center
        activityIndicator.startAnimating()
    }
}

