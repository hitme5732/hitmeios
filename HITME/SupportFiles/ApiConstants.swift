//
//  Constants.swift
//  HITME
//
//  Created by kiran kumar on 27/08/21.
//  Copyright © 2021 kiran kumar. All rights reserved.
//

import Foundation
import Alamofire

struct AppURL {
    static let baseURL = "https://hitme.co.in/"
    static let token = baseURL + "oauth/token"
    static let registration = baseURL + "api/userregistration"
    static let login = baseURL + "api/checkuserlogin"
    static let updateProfile = baseURL + "api/updateprofile"
    static let changePassword = baseURL + "api/changepassword"
}

class Connectivity {
    class func isConnectedToInternet() -> Bool {
        return NetworkReachabilityManager()?.isReachable ?? false
    }
}
