//
//  UI+Extensions.swift
//  HITME
//
//  Created by Rahul Sharma on 29/09/21.
//

import UIKit

protocol Reusable: AnyObject {
    static var reuseIdentifier:String { get }
    static var nib:UINib? { get }
}

extension Reusable{
    static var reuseIdentifier:String {
        return String(describing: self)
    }
    static var nib:UINib? {
        return UINib(nibName: String(describing: self), bundle: nil)
    }
}

/// Reusable Code For StoryBoard Instantiation
protocol Storyboarded {
    static func instantiate(storyBoardName name:String) -> Self
}

extension Storyboarded where Self: UIViewController {
    static func instantiate(storyBoardName name:String) -> Self {
        let storyboard = UIStoryboard(name: name, bundle: Bundle.main)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! Self
    }
}

/// Reusable Code For TableView Cell
extension UITableViewCell:Reusable{}

/// Reusable code for CollectionViewCell
extension UICollectionViewCell:Reusable{}

extension UIViewController:Storyboarded{}
