//
//  Helper.swift
//  RAYT
//
//  Created by SAIKUMAR on 16/06/21.
//

import Foundation
import UIKit

class Helper:NSObject {
    
    class func setViewBorder(view:UIView,radius:CGFloat,color:UIColor) {
        view.layer.borderWidth = 1.0
        view.layer.borderColor = color.cgColor
        view.layer.cornerRadius = radius
    }
    
    class func UIColorFromRGB(rgbValue: UInt) -> UIColor {
              return UIColor(
                  red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
                  green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
                  blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
                  alpha: CGFloat(1.0)
              )
          }
          class func addShadow(view:UIView,color:UIColor,shadowOpacity:Float,shadowRadius:CGFloat) {
          //    view.layer.borderColor = color.cgColor
              // shadow
              view.layer.masksToBounds = false
              view.layer.shadowColor = color.cgColor
              view.layer.shadowOffset = CGSize(width: 1, height: 1)
              view.layer.shadowOpacity = shadowOpacity
              view.layer.shadowRadius = shadowRadius
          }

}

@IBDesignable extension UIView {
    @IBInspectable var borderColor:UIColor? {
        set {
            layer.borderColor = newValue!.cgColor
        }
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor:color)
            }
            else {
                return nil
            }
        }
    }
    @IBInspectable var borderWidth:CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    @IBInspectable var cornerRadius:CGFloat {
        set {
            layer.cornerRadius = newValue
            clipsToBounds = newValue > 0
        }
        get {
            return layer.cornerRadius
        }
    }
}


@IBDesignable extension UIButton {

    @IBInspectable override var borderWidth: CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }

    @IBInspectable override var cornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
        }
        get {
            return layer.cornerRadius
        }
    }

    @IBInspectable override var borderColor: UIColor? {
        set {
            guard let uiColor = newValue else { return }
            layer.borderColor = uiColor.cgColor
        }
        get {
            guard let color = layer.borderColor else { return nil }
            return UIColor(cgColor: color)
        }
    }
}

// MARK: ADDING IMAGE TO TEXTVIEW

extension UITextField {
    func addpaddingWithImage(_ img: UIImage) {
        self.rightViewMode = .always
        
        let imageRight : UIImageView = UIImageView(image: img)
        imageRight.frame = CGRect.init(origin: CGPoint.init(x: 0, y: 0), size: CGSize.init(width: 20, height: 20))
        imageRight.contentMode = .scaleAspectFit
        
        let viewRight : UIView = UIView.init(frame: CGRect.init(origin: CGPoint.init(x: 0, y: 0), size: CGSize.init(width: 38, height: 38)))
        imageRight.center = viewRight.center
        viewRight.addSubview(imageRight)

        self.rightView = viewRight
    }
}

extension UIView {

    //cut irrelevant code for SO Question

    @IBInspectable
    var masksToBounds: Bool {
        get {
            return layer.masksToBounds
        }
        set {
            layer.masksToBounds = newValue
        }
    }

    // Shadow handling
    @IBInspectable
    var shadowColor: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
    }

    @IBInspectable
    var shadowOpacity: Float {
        get {
            return layer.opacity
        }
        set {
            layer.opacity = newValue
        }
    }


    @IBInspectable
    var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }

    @IBInspectable
    var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }


}



extension UIView
{

    func cornerRadiusForView()
    {
        self.layer.masksToBounds = false
        self.layer.cornerRadius = 10.0
        self.layer.shadowOffset = CGSize(width: -1, height:1)

        self.layer.shadowOpacity = 0.1
    }
}


extension UIView
{

    func circlularRadiusForView1(radius:Float)
    {
        self.layer.masksToBounds = false
        self.layer.cornerRadius = CGFloat(radius)
      //  self.layer.masksToBounds = true
        self.layer.shadowOffset = CGSize(width: -0.2, height:0.2)
        self.layer.shadowOpacity = 0.3
    }
}
extension UIView
{

    func cornerradiuswithoutshadow(radius:Float)
    {
        self.layer.masksToBounds = false
        self.layer.cornerRadius = CGFloat(radius)
        //  self.layer.shadowOffset = CGSize(width: -1, height:1)
        //   self.layer.shadowOpacity = 0.7
    }
}




extension UILabel {
  func set(html: String) {
    if let htmlData = html.data(using: .unicode) {
      do {
        self.attributedText = try NSAttributedString(data: htmlData,
                                                     options: [.documentType: NSAttributedString.DocumentType.html],
                                                     documentAttributes: nil)
      } catch let e as NSError {
        print("Couldn't parse \(html): \(e.localizedDescription)")
      }
    }
  }
}
@nonobjc extension UIViewController {
    func add(_ child: UIViewController, frame: CGRect? = nil) {
        addChild(child)

        if let frame = frame {
            child.view.frame = frame
        }

        view.addSubview(child.view)
        child.didMove(toParent: self)
    }

    func remove() {

        print("Remove Screen")
        willMove(toParent: nil)
        view.removeFromSuperview()
        removeFromParent()
    }

}

extension UITableView {

    func reloadWithoutAnimation() {
        let lastScrollOffset = contentOffset
        beginUpdates()
        layer.removeAllAnimations()
        setContentOffset(lastScrollOffset, animated: false)
        reloadData()
        endUpdates()
    }
}
// ALL Extension Here

extension UIViewController {

        func getFormattedDate(string: String , formatter:String) -> String{
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"

            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "MMM dd,yyyy"

            let date: Date? = dateFormatterGet.date(from: "2018-02-01T19:10:04+00:00")
            print("Date",dateFormatterPrint.string(from: date!)) // Feb 01,2018
            return dateFormatterPrint.string(from: date!);
        }


    func toast(message : String, font: UIFont) {
        
        let toastLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 100))
        toastLabel.backgroundColor = UIColor.orange
        toastLabel.textColor = UIColor.white
        toastLabel.font = font
        toastLabel.textAlignment = .center;
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 0;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: [.curveEaseOut, .transitionCurlDown], animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    } 
    func showToast(message : String) {

        let message = message
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)

        self.present(alert, animated: true)

        let duration: Double = 2

        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + duration) {
            alert.dismiss(animated: true)
        }
    }
    func customPresentAlert(withTitle title: String, message : String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title:  "OK", style: .default) { action in
            print("You've pressed OK Button")
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)


    }

    func reloadViewFromNib() {
        let parent = view.superview
        view.removeFromSuperview()
        view = nil
        parent?.addSubview(view)
    }


    func showLoginAlert() {

        let alert = UIAlertController(title: "Alert", message: "Please login to your acccount",    preferredStyle: UIAlertController.Style.alert)

        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { _ in

    }))
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: {(_: UIAlertAction!) in


//     let vc = UIStoryboard.init(name: "Profile", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginVC") as? LoginVC
//      self.navigationController?.pushViewController(vc!, animated: true)

    })) 
    self.present(alert, animated: true, completion: nil)
       }
        func socialmedia(mediaurl : String){
        let appURL = URL(string: mediaurl)!

        if #available(iOS 10.0, *) {
        UIApplication.shared.open(appURL as URL, options: [:], completionHandler: nil)
        }
        else {
        UIApplication.shared.openURL(appURL as URL)
        }

    }


    func applyShadowOnView(_ view:UIView) {

            view.layer.cornerRadius = 8
            view.layer.shadowColor = UIColor.lightGray.cgColor
            view.layer.shadowOpacity = 1
            view.layer.shadowOffset = CGSize.zero
            view.layer.shadowRadius = 5

    }


}
// assign Service Image string file to uiimageview
extension UIImage {
    convenience init?(url: URL?) {
        guard let url = url else { return nil }

        do {
            let data = try Data(contentsOf: url)
            self.init(data: data)
        } catch {
            print("Cannot load image from url: \(url) with error: \(error)")
            return nil
        }
  }
}
// useage profilePicImageView.image = UIImage.init(url: URL.init(string: profilePath!))

extension String {
    func isValidEmailCondition() -> Bool {
        // here, `try!` will always succeed because the pattern is valid
        let regex = try! NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)$", options: .caseInsensitive)
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
    }
}
extension String {
func isValidPhoneNumber(phoneNumber: String) -> Bool {
   let phoneNumberRegex = "^[8]\\d{8}$"
   let trimmedString = phoneNumber.trimmingCharacters(in: .whitespaces)
   let validatePhone = NSPredicate(format: "SELF MATCHES %@", phoneNumberRegex)
   let isValidPhone = validatePhone.evaluate(with: trimmedString)
   return isValidPhone
}
}
/* Removing HTML Tags */
extension String {
    public var withoutHtml: String {
        guard let data = self.data(using: .utf8) else {
            return self
        }

        let options: [NSAttributedString.DocumentReadingOptionKey: Any] = [
            .documentType: NSAttributedString.DocumentType.html,
            .characterEncoding: String.Encoding.utf8.rawValue
        ]

        guard let attributedString = try? NSAttributedString(data: data, options: options, documentAttributes: nil) else {
            return self
        }

        return attributedString.string
    }
}
class Validation {
public func validateName(name: String) ->Bool {
   // Length be 18 characters max and 3 characters minimum, you can always modify.
   let nameRegex = "^\\w{3,18}$"
   let trimmedString = name.trimmingCharacters(in: .whitespaces)
   let validateName = NSPredicate(format: "SELF MATCHES %@", nameRegex)
   let isValidateName = validateName.evaluate(with: trimmedString)
   return isValidateName
}
public func validaPhoneNumber(phoneNumber: String) -> Bool {
   let phoneNumberRegex = "^[8-8]\\d{8}$"
   let trimmedString = phoneNumber.trimmingCharacters(in: .whitespaces)
   let validatePhone = NSPredicate(format: "SELF MATCHES %@", phoneNumberRegex)
   let isValidPhone = validatePhone.evaluate(with: trimmedString)
   return isValidPhone
}
public func validateEmailId(emailID: String) -> Bool {
   let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
   let trimmedString = emailID.trimmingCharacters(in: .whitespaces)
   let validateEmail = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
   let isValidateEmail = validateEmail.evaluate(with: trimmedString)
   return isValidateEmail
}
public func validatePassword(password: String) -> Bool {
   //Minimum 8 characters at least 1 Alphabet and 1 Number:
   let passRegEx = "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$"
   let trimmedString = password.trimmingCharacters(in: .whitespaces)
   let validatePassord = NSPredicate(format:"SELF MATCHES %@", passRegEx)
   let isvalidatePass = validatePassord.evaluate(with: trimmedString)
   return isvalidatePass
}
public func validateAnyOtherTextField(otherField: String) -> Bool {
      let otherRegexString = "Your regex String"
      let trimmedString = otherField.trimmingCharacters(in: .whitespaces)
      let validateOtherString = NSPredicate(format: "SELF MATCHES %@", otherRegexString)
      let isValidateOtherString = validateOtherString.evaluate(with: trimmedString)
      return isValidateOtherString
   }
    public func setAlertView(alert:String,msg:String,controller:UIViewController) {
        let alert = UIAlertController(title: alert, message: msg, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        controller.present(alert, animated: true, completion: nil)
    }
}
