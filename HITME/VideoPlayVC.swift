//
//  VideoPlayVC.swift
//  ProfileScreens
//
//  Created by Naresh Oruganti on 04/10/21.
//

import UIKit
import AVKit
import AVFoundation


class VideoPlayVC: UIViewController {
    var avpController = AVPlayerViewController()
    @IBOutlet weak var videoPlayView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        playVideo()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    private func playVideo() {
        guard let path = Bundle.main.path(forResource: "1-TextEdit", ofType:"mov") else {
            debugPrint("video.m4v not found")
            return
        }
        let player = AVPlayer(url: URL(fileURLWithPath: path))
        
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = self.videoPlayView.bounds
        
        avpController.view.frame.size.height = videoPlayView.frame.size.height
        
        avpController.view.frame.size.width = videoPlayView.frame.size.width
        self.videoPlayView.layer.addSublayer(playerLayer)
        player.play()
    }
    
}
