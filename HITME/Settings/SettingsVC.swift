//
//  SettingsVC.swift
//  HITME
//
//  Created by SAI KUMAR on 08/10/21.
//

import UIKit

class SettingsVC: UIViewController {

    @IBOutlet weak var settingsTableView: UITableView!
    var settingArray = ["Change Font Style","Rate Hitme","Share Hitme","Faq","About Hitme"]
    override func viewDidLoad() {
        super.viewDidLoad()



    }
    
    @IBAction func backBtnTapped(_ sender: UIButton) {
    }
}
extension SettingsVC:UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return settingArray.count
        
        }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsTVC") as? SettingsTVC else {
            return UITableViewCell()
        }
        cell.headingLbl.text = settingArray[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
