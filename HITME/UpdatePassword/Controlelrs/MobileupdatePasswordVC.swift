//
//  MobileupdatePasswordVC.swift
//  HITME
//
//  Created by Naresh Oruganti on 05/10/21.
//

import UIKit

class MobileupdatePasswordVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func verifyAction(_ sender: Any) {
        let screenRef = self.storyboard?.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
        screenRef.isComingFromUpdate = true
        self.navigationController?.pushViewController(screenRef, animated: false)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
