//
//  FollowingVC.swift
//  HITME
//
//  Created by Naresh Oruganti on 29/09/21.
//

import UIKit

class FollowingVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
      
        // Do any additional setup after loading the view.
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        // Show the navigation bar on other view controllers
        self.navigationController?.navigationBar.backgroundColor = .orange
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
}
extension FollowingVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "FollowingTVC") as? FollowingTVC else {
            return UITableViewCell()
        }
        cell.showAlert.addTarget(self, action: #selector(connected(sender:)), for: .touchUpInside)

        return cell
    }
    @objc func connected(sender: UIButton){
        let alert = UIAlertController(title: "Message", message: "Please Choose an Option", preferredStyle: .actionSheet)
           
           alert.addAction(UIAlertAction(title: "Unfollow", style: .default , handler:{ (UIAlertAction)in
               print("User click Approve button")
           }))
           
           alert.addAction(UIAlertAction(title: "Block report", style: .default , handler:{ (UIAlertAction)in
               print("User click Edit button")
           }))

           //uncomment for iPad Support
           //alert.popoverPresentationController?.sourceView = self.view

           self.present(alert, animated: true, completion: {
               print("completion block")
           })
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
