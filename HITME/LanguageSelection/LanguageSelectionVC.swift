//
//  LanguageSelectionVC.swift
//  HITME
//
//  Created by SAIKUMAR on 27/09/21.
//

import UIKit

class LanguageSelectionVC: UIViewController {

    @IBOutlet weak var langTableView: UITableView!
    var cellData: [CellItem] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        cellData = [CellItem(name: "Hindi"),CellItem(name: "English"),CellItem(name: "Tamil"),CellItem(name: "Malyalam"),CellItem(name: "Kannad"),CellItem(name: "Marathi")]
    }
    

    @IBAction func backBtnTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func applyBtnTapped(_ sender: UIButton) {
        let home = ReelsContainerVC.instantiate(storyBoardName: "Home")
        self.navigationController?.pushViewController(home, animated: true)
    }
}
extension LanguageSelectionVC:UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return cellData.count
        
        }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "LanguageSelectionTVC") as? LanguageSelectionTVC else {
            return UITableViewCell()
        }
        cell.contLbl.text = cellData[indexPath.row].name
        if cellData[indexPath.row].isSelected == false {
            cell.selectImg.image = #imageLiteral(resourceName: "Rectangle 139")
        }else {
            cell.selectImg.image = #imageLiteral(resourceName: "Group 252")
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 54
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if cellData[indexPath.row].isSelected == false {
            cellData[indexPath.row].isSelected = true
        }else {
            cellData[indexPath.row].isSelected = false
        }
        DispatchQueue.main.async {
            self.langTableView.reloadData()
        }
    }
}
struct CellItem {
    var name : String
    var isSelected:Bool! = false
    init(name: String) {
        self.name = name
    }
}
