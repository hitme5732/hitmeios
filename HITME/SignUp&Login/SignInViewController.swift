//
//  ViewController.swift
//  HITME
//
//  Created by SAIKUMAR on 25/09/21.
//

import UIKit
import Alamofire

class SignInViewController: UIViewController {

    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var mobileNumberTF: UITextField!
   // @IBOutlet weak var newUserSignUpLbl: UILabel!
    var validation = Validation()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupUI()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.view.removeBluerLoader()
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    
    @IBAction func skipAction(_ sender: Any) {
        let home = ReelsContainerVC.instantiate(storyBoardName: "Home")
        self.navigationController?.pushViewController(home, animated: true)
    }
    
    
    
    func setupUI() {
        self.passwordTF.addpaddingWithImage(UIImage(named: "password")!)
        self.emailTF.addpaddingWithImage(UIImage(named: "email")!)
        self.mobileNumberTF.addpaddingWithImage(UIImage(named: "mobile-number")!)
        
        let newUserStr = NSAttributedString(string: "New User?")
        let attributes = [NSAttributedString.Key.font:UIFont.boldSystemFont(ofSize: 17)]
        let signUpStr = NSAttributedString(string: " Sign Up", attributes: attributes)
        let appendStr = NSMutableAttributedString()
        appendStr.append(newUserStr)
        appendStr.append(signUpStr)
     //   self.newUserSignUpLbl.attributedText = appendStr
        
       // self.newUserSignUpLbl.isUserInteractionEnabled = true
    //    self.newUserSignUpLbl.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTappedNewUserLbl)))
    }
    
    @objc func handleTappedNewUserLbl() {
        let screenRef = self.storyboard?.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        self.navigationController?.pushViewController(screenRef, animated: true)
    }
    
    @IBAction func signupAction(_ sender: Any) {
    
            let screenRef = self.storyboard?.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
            self.navigationController?.pushViewController(screenRef, animated: true)
        
    }
    
    @IBAction func signinButtonAction(_ sender: Any) {
        if self.emailTF.text!.count > 0 &&  self.passwordTF.text!.count > 0 {
            self.view.showBlurLoader()
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                let screenRef = self.storyboard?.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
                self.navigationController?.pushViewController(screenRef, animated: true)
            }
        }else {
            validation.setAlertView(alert:"Message",msg:"Please check valid credentials",controller:self)
        }
    }
    
    
    @IBAction func otpBtnAction(_ sender: Any) {
        if self.mobileNumberTF.text?.count == 10 {
            self.mobileNumberTF.resignFirstResponder()
            let screenRef = self.storyboard?.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
            self.navigationController?.pushViewController(screenRef, animated: true)
        }else {
            validation.setAlertView(alert:"Message",msg:"Please check your mobile number",controller:self)
        }

    }
    
// MARK: SignInApiCall
    
    func SignInApiCall(){
        if Reachability.isConnectedToNetwork() {
            ServicesHelper.sharedInstance.loader(view: self.view)
            
            let postDict = [
                "email":emailTF.text as Any,
                "password":emailTF.text as Any,
                
            ] as [String : Any]
            let Urlname = ApiNames.liveURL + "api/checkuserlogin"
            ServicesHelper.sharedInstance.postServiceCall(url:Urlname , PostDetails: postDict as [String : AnyObject]) { [self] (response, error)  in
                if let res =  response["error"] as? String {
                    if res == "Server Error" {
                        self.customPresentAlert(withTitle: "Alert", message:"Server Error")
                        ServicesHelper.sharedInstance.dissMissLoader()
                        return
                    }
                }
                print("Response of saveCardAPI ",response)
                let status  = (response["status"] as! String)
                ServicesHelper.sharedInstance.dissMissLoader()
                //let message = responseData["message"] as! String
                if status == "2"{
                    let msg = response["status_messsage"] as! String
                    //                    let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "OTPVerificationVC") as? OTPVerificationVC
                    UserDefaults.standard.setValue(emailTF.text, forKey: "user_mail")
                    //   self.navigationController?.pushViewController(vc!, animated: true)
                    self.customPresentAlert(withTitle: "", message:msg)
                }
                
                else{
                    
                    let msg = response["status_messsage"] as! String
                    self.customPresentAlert(withTitle: "", message:msg)
                }
                DispatchQueue.main.async {
                }
                
            }} else {
                ServicesHelper.sharedInstance.dissMissLoader()
                self.customPresentAlert(withTitle: "", message: "Please check your Internet Connection")
                
            }}

    
    // MARK: TokenApiCall
        
    func TokenApiCall(){
        if Reachability.isConnectedToNetwork() {
            ServicesHelper.sharedInstance.loader(view: self.view)
            
            let postDict = [
                "username":emailTF.text as Any,
                "password":emailTF.text as Any,
                "grant_type": "",
                "client_id": "",
                "client_secret": "",
                
            ] as [String : Any]
            let Urlname = ApiNames.liveURL + "oauth/token"
            ServicesHelper.sharedInstance.postServiceCall(url:Urlname , PostDetails: postDict as [String : AnyObject]) { [self] (response, error)  in
                if let res =  response["error"] as? String {
                    if res == "Server Error" {
                        self.customPresentAlert(withTitle: "Alert", message:"Server Error")
                        ServicesHelper.sharedInstance.dissMissLoader()
                        return
                    }
                }
                print("Response of saveCardAPI ",response)
                let status  = (response["status"] as! String)
                ServicesHelper.sharedInstance.dissMissLoader()
                //let message = responseData["message"] as! String
                if status == "2"{
                    let msg = response["status_messsage"] as! String
                    //                    let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "OTPVerificationVC") as? OTPVerificationVC
                    //   self.navigationController?.pushViewController(vc!, animated: true)
                    self.customPresentAlert(withTitle: "", message:msg)
                }
                
                else{
                    
                    let msg = response["status_messsage"] as! String
                    self.customPresentAlert(withTitle: "", message:msg)
                }
                DispatchQueue.main.async {
                }
                
            }} else {
                ServicesHelper.sharedInstance.dissMissLoader()
                self.customPresentAlert(withTitle: "", message: "Please check your Internet Connection")
                
            }}

}
