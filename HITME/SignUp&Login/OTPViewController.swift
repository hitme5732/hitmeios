//
//  ViewController.swift
//  HITME
//
//  Created by SAIKUMAR on 25/09/21.
//


import UIKit
import OTPFieldView

class OTPViewController: UIViewController {
    var isComingFromUpdate:Bool = false
    @IBOutlet weak var otpFieldView: OTPFieldView!
    @IBOutlet weak var requestAgainLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.removeBluerLoader()
        self.setupUI()
        
    }
    
    func setupUI() {
        self.otpFieldView.fieldsCount = 4
        self.otpFieldView.cursorColor = .red
        self.otpFieldView.displayType = .roundedCorner
        self.otpFieldView.separatorSpace = 10
        self.otpFieldView.defaultBackgroundColor = .white
        self.otpFieldView.filledBackgroundColor = .white
        self.otpFieldView.fieldSize = 50
        self.otpFieldView.defaultBorderColor = .white
        self.otpFieldView.delegate = self
        self.otpFieldView.initializeUI()
        
        let newUserStr = NSAttributedString(string: "Didn't receive code?")
        let attributes = [NSAttributedString.Key.font:UIFont.boldSystemFont(ofSize: 17)]
        let signUpStr = NSAttributedString(string: " Request Again", attributes: attributes)
        let appendStr = NSMutableAttributedString()
        appendStr.append(newUserStr)
        appendStr.append(signUpStr)
        self.requestAgainLbl.attributedText = appendStr
        
        self.requestAgainLbl.isUserInteractionEnabled = true
        self.requestAgainLbl.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTappedRequestAgainLbl)))
    }
    
    @objc func handleTappedRequestAgainLbl() {
        print("tapped on label..")
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submitAction(_ sender: Any) {
        if isComingFromUpdate {
            let screenRef = self.storyboard?.instantiateViewController(withIdentifier: "ChnagePasswordVC") as! ChnagePasswordVC
            self.navigationController?.pushViewController(screenRef, animated: true)
        }else {
            let screenRef = self.storyboard?.instantiateViewController(withIdentifier: "LanguageSelectionVC") as! LanguageSelectionVC
            self.navigationController?.pushViewController(screenRef, animated: true)
        }
        
    }
    
}

extension OTPViewController:OTPFieldViewDelegate {
    func shouldBecomeFirstResponderForOTP(otpTextFieldIndex index: Int) -> Bool {
        return true
    }
    
    func enteredOTP(otp: String) {
        print(otp)
    }
    
    func hasEnteredAllOTP(hasEnteredAll: Bool) -> Bool {
        print("has entered all OTP \(hasEnteredAll)")
        return false
    }
}
