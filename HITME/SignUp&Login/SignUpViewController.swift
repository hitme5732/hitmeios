//
//  ViewController.swift
//  HITME
//
//  Created by SAIKUMAR on 25/09/21.
//

import UIKit

class SignUpViewController: UIViewController {
    
    @IBOutlet weak var signInLbl: UILabel!
    @IBOutlet weak var confirmPasswordTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var termsBtnRef: UIButton!
    @IBOutlet weak var femaleBtnRef: UIButton!
    @IBOutlet weak var maleBtnRef: UIButton!
    @IBOutlet weak var dobTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var mobileNumberTF: UITextField!
    @IBOutlet weak var profleNameTF: UITextField!
    @IBOutlet weak var lastNameTF: UITextField!
    @IBOutlet weak var firstNameTF: UITextField!
    @IBOutlet weak var profileImgRef: UIImageView!
    
    @IBOutlet weak var datePick: UIDatePicker!
    @IBOutlet weak var bottomDatePicker: NSLayoutConstraint!
    @IBOutlet weak var femaleImageCheck: UIImageView!
    @IBOutlet weak var maleImageCheck: UIImageView!

    var validation = Validation()
    
    var imagePicker = UIImagePickerController()
    var genderType = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.datePick.backgroundColor = .white
        datePick.maximumDate = Date()
        self.bottomDatePicker.constant = -400
        self.dobTF.isUserInteractionEnabled = false
        self.view.removeBluerLoader()
        self.termsBtnRef.setImage(UIImage.init(named: "off"), for: UIControl.State.normal)
        self.termsBtnRef.isSelected = false
        self.termsBtnRef.tag = 1
        self.setupUI()
        //  SignUpApiCall()
        //  ServicesHelper.sharedInstance.loader(view: self.view)
        //self.view.showBlurLoader()
    }
    
    func setupUI() {
        
        self.imagePicker.delegate = self
        self.imagePicker.allowsEditing = true
        
        self.profileImgRef.contentMode = .scaleAspectFill
        
        self.firstNameTF.addpaddingWithImage(UIImage(named: "profile_icon")!)
        self.lastNameTF.addpaddingWithImage(UIImage(named: "profile_icon")!)
        self.profleNameTF.addpaddingWithImage(UIImage(named: "profile_icon")!)
        self.mobileNumberTF.addpaddingWithImage(UIImage(named: "mobile-number")!)
        self.dobTF.addpaddingWithImage(UIImage(named: "calendar")!)
        self.emailTF.addpaddingWithImage(UIImage(named: "email")!)
        //   self.passwordTF.addpaddingWithImage(UIImage(named: "password")!)
        // self.confirmPasswordTF.addpaddingWithImage(UIImage(named: "password")!)
        
        let newUserStr = NSAttributedString(string: "Already user?")
        let attributes = [NSAttributedString.Key.font:UIFont.boldSystemFont(ofSize: 17)]
        let signUpStr = NSAttributedString(string: " Sign In", attributes: attributes)
        let appendStr = NSMutableAttributedString()
        appendStr.append(newUserStr)
        appendStr.append(signUpStr)
        self.signInLbl.attributedText = appendStr
        
        self.signInLbl.isUserInteractionEnabled = true
        self.signInLbl.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTappedOnSignInLbl)))
    }
    
    @objc func handleTappedOnSignInLbl() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func doneAction(_ sender: Any) {
        self.bottomDatePicker.constant = -400
        let formatter = DateFormatter()
          formatter.dateFormat = "dd/MM/yyyy"
        self.dobTF.text = formatter.string(from: datePick.date)
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        self.bottomDatePicker.constant = -400
    }
    
    @IBAction func signupAction(_ sender: Any) {
        
        
        
        if  firstNameTF.text == "" {
            validation.setAlertView(alert:"Alert",msg:"Please Enter Your First Name",controller:self)
        }else if  lastNameTF.text == "" {
            validation.setAlertView(alert:"Alert",msg:"Please Enter Your Last Name",controller:self)
        }
        else if profleNameTF.text == "" {
            validation.setAlertView(alert:"Alert",msg:"Please Enter Your Profile Name",controller:self)
            
        }
        else if mobileNumberTF.text!.count != 10 {
            validation.setAlertView(alert:"Alert",msg:"Please Enter Correct Mobile Number",controller:self)
        }
        
        else if emailTF.text == ""{
            validation.setAlertView(alert:"Alert",msg:" Please Enter Your Email Address ",controller:self)
        }else if dobTF.text == ""{
            validation.setAlertView(alert:"Alert",msg:" Please Enter Your Date of Birth ",controller:self)
        }
        else if passwordTF.text == "" {
            validation.setAlertView(alert:"Alert",msg:" Please Enter Your Password ",controller:self)
            
        }else if confirmPasswordTF.text == "" {
            validation.setAlertView(alert:"Alert",msg:" Please Enter Your Confirm Password ",controller:self)
            
        }else if passwordTF.text!.count < 6 {
            validation.setAlertView(alert:"Alert",msg:"Password Should Contain Minimum 6 characters",controller:self)
        }
        else if passwordTF.text != confirmPasswordTF.text {
            
            validation.setAlertView(alert:"Alert",msg:"Password Not matching ",controller:self)
        }
        else if self.termsBtnRef.isSelected == false {
            validation.setAlertView(alert:"Alert",msg:"Accept Terms and Conditions ",controller:self)
        }else{
            let screenRef = self.storyboard?.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
            self.navigationController?.pushViewController(screenRef, animated: true)
        }
    }
    
    @IBAction func dobAction(_ sender: Any) {
        self.bottomDatePicker.constant = 0
    }
    
    
    
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func imagePickerBtnAction(_ sender: Any) {
        let alert = UIAlertController(title: "Select an action\nFrom", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { (galleryBtn) in
            self.openGallery()
        }))
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (cameraBtn) in
            self.openCamera()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (cancelBtn) in
            
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera() {
        self.imagePicker.sourceType = .camera
        self.present(self.imagePicker, animated: true, completion: nil)
    }
    
    func openGallery() {
        self.imagePicker.sourceType = .photoLibrary
        self.present(self.imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func maleBtAction(_ sender: Any) {
        self.maleImageCheck.image = #imageLiteral(resourceName: "Group 252")
        self.maleImageCheck.contentMode = .scaleAspectFit
        self.femaleImageCheck.contentMode = .scaleAspectFit
        self.femaleImageCheck.image = #imageLiteral(resourceName: "Rectangle 174")
        genderType = "Male"
        
    }
    
    @IBAction func femaleBtnAction(_ sender: Any) {
        self.maleImageCheck.image = #imageLiteral(resourceName: "Rectangle 174")
        self.maleImageCheck.contentMode = .scaleAspectFit
        self.femaleImageCheck.contentMode = .scaleAspectFit
        self.femaleImageCheck.image = #imageLiteral(resourceName: "Group 252")
        genderType = "FeMale"
        
    }
    
    
    @IBAction func tcAction(_ sender: Any) {
        let screenRef = self.storyboard?.instantiateViewController(withIdentifier: "TCVC") as! TCVC
        self.navigationController?.present(screenRef, animated: false, completion: nil)
    }
    
    
    
    
    
    @IBAction func termsAndConditionsBtnAction(_ sender: Any) {
        
        if self.termsBtnRef.tag == 1 {
            self.termsBtnRef.setImage(UIImage.init(named: "on"), for: UIControl.State.normal)
            self.termsBtnRef.tag = 2
            self.termsBtnRef.isSelected = true
        }
        else{
            self.termsBtnRef.setImage(UIImage.init(named: "off"), for: UIControl.State.normal)
            self.termsBtnRef.tag = 1
            self.termsBtnRef.isSelected = false
        }
        
        
    }
    // MARK: SignUpApiCall
    
    func SignUpApiCall(){
        if Reachability.isConnectedToNetwork() {
            ServicesHelper.sharedInstance.loader(view: self.view)
            
            
            let postDict = [
                "first_name":"Vijay",
                "last_name":"Bhaskar",
                "profile_id":"vijay1234-22",
                "mobile_number":"9000393611",
                "email":"vijay@gac.com",
                "date_of_birth":"2001-08-24",
                "gender":"male",
                "password":"Test@123",
                
            ] as [String : Any]
            let Urlname = ApiNames.liveURL + "api/userregistration"
            ServicesHelper.sharedInstance.postServiceCall(url:Urlname , PostDetails: postDict as [String : AnyObject]) { [self] (response, error)  in
                if let res =  response["error"] as? String {
                    if res == "Server Error" {
                        self.customPresentAlert(withTitle: "Alert", message:"Server Error")
                        ServicesHelper.sharedInstance.dissMissLoader()
                        return
                    }
                }
                print("Response of saveCardAPI ",response)
                let status  = (response["status"] as! String)
                ServicesHelper.sharedInstance.dissMissLoader()
                //let message = responseData["message"] as! String
                if status == "2"{
                    let msg = response["status_messsage"] as! String
                    let screenRef = self.storyboard?.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
                    UserDefaults.standard.setValue(firstNameTF.text, forKey: "user_Fname")
                    UserDefaults.standard.setValue(lastNameTF.text, forKey: "user_Lname")
                    UserDefaults.standard.setValue(profleNameTF.text, forKey: "user_profileId")
                    UserDefaults.standard.setValue(mobileNumberTF.text, forKey: "user_Mobile")
                    UserDefaults.standard.setValue(emailTF.text, forKey: "user_mail")
                    UserDefaults.standard.setValue(genderType, forKey: "user_gender")
                    UserDefaults.standard.setValue(dobTF.text, forKey: "user_Dob")
                    //         UserDefaults.standard.setValue(userId, forKey: "user_Id")
                    self.navigationController?.pushViewController(screenRef, animated: true)
                    self.customPresentAlert(withTitle: "", message:msg)
                }
                
                else{
                    let msg = response["status_messsage"] as! String
                    self.customPresentAlert(withTitle: "", message:msg)
                }
                DispatchQueue.main.async {
                }
                
            }} else {
                ServicesHelper.sharedInstance.dissMissLoader()
                self.customPresentAlert(withTitle: "", message: "Please check your Internet Connection")
                
            }}
    
    
}


extension SignUpViewController:UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let editedImage = info[.editedImage] as? UIImage {
            self.profileImgRef.image = editedImage
        }else if let originalImage = info[.originalImage] as? UIImage {
            self.profileImgRef.image = originalImage
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}
