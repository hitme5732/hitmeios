//
//  ChnagePasswordVC.swift
//  HITME
//
//  Created by SAI KUMAR on 01/10/21.
//

import UIKit

class ChnagePasswordVC: UIViewController {

    @IBOutlet weak var newPasswordTf: UITextField!
    @IBOutlet weak var confirmPasswordTf: UITextField!
    var validation = Validation()
    override func viewDidLoad() {
        super.viewDidLoad()


    }
   
    @IBAction func skipBtnTapped(_ sender: Any) {
       
        let home = ReelsContainerVC.instantiate(storyBoardName: "Home")
        self.navigationController?.pushViewController(home, animated: true)
    }
    @IBAction func BackBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
        
    }
    @IBAction func changePasswordBtnTapped(_ sender: Any) {
        
//         if newPasswordTf.text == "" {
//            validation.setAlertView(alert:"Alert",msg:"Please Enter Your Password",controller:self)
//
//        }else if confirmPasswordTf.text == "" {
//            validation.setAlertView(alert:"Alert",msg:"Please Enter Your Confirm Password",controller:self)
//
//        }else if newPasswordTf.text!.count < 6 {
//            validation.setAlertView(alert:"Alert",msg:"Password Should Contain Minimum 6 characters",controller:self)
//        }
//        else if newPasswordTf.text != confirmPasswordTf.text {
//
//            validation.setAlertView(alert:"Alert",msg:"Password Not matching",controller:self)
//        }else{
//            ChangePasswordApiCall()
//        }
//
        
        let home = ReelsContainerVC.instantiate(storyBoardName: "Home")
        self.navigationController?.pushViewController(home, animated: true)
    }


    // MARK: ChangePasswordApiCall
        
        func ChangePasswordApiCall(){
            if Reachability.isConnectedToNetwork() {
                ServicesHelper.sharedInstance.loader(view: self.view)

                let postDict = [
                    "user_id":UserDefaults.standard.string(forKey: "user_id") as Any,
                                 "password":newPasswordTf.text as Any,
                                 
                ] as [String : Any]
                let Urlname = ApiNames.liveURL + "api/changepassword"
                ServicesHelper.sharedInstance.postServiceCall(url:Urlname , PostDetails: postDict as [String : AnyObject]) { [self] (response, error)  in
                    if let res =  response["error"] as? String {
                        if res == "Server Error" {
                        self.customPresentAlert(withTitle: "Alert", message:"Server Error")
                            ServicesHelper.sharedInstance.dissMissLoader()
                            return
                        }
                    }
                    print("Response of saveCardAPI ",response)
                    let status  = (response["status"] as! String)
                    ServicesHelper.sharedInstance.dissMissLoader()
                    //let message = responseData["message"] as! String
                    if status == "2"{
                        let msg = response["status_messsage"] as! String
    //                    let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "OTPVerificationVC") as? OTPVerificationVC
                       // UserDefaults.standard.setValue(dnfff, forKey: "user_id")
                     //   self.navigationController?.pushViewController(vc!, animated: true)
                        self.customPresentAlert(withTitle: "", message:msg)
                    }
                    
                    else{
                        
                        let msg = response["status_messsage"] as! String
                        self.customPresentAlert(withTitle: "", message:msg)
                    }
                    DispatchQueue.main.async {
                    }
                    
                }} else {
                    ServicesHelper.sharedInstance.dissMissLoader()
                    self.customPresentAlert(withTitle: "", message: "Please check your Internet Connection")
                    
                }}

    }

