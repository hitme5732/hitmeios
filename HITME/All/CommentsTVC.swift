//
//  CommentsTVC.swift
//  HITME
//
//  Created by SAI KUMAR on 29/09/21.
//

import UIKit

class CommentsTVC: UITableViewCell {

    @IBOutlet weak var infoLbl: UILabel!
    @IBOutlet weak var duetDetailsLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
