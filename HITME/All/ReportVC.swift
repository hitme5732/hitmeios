//
//  ReportVC.swift
//  HITME
//
//  Created by SAI KUMAR on 29/09/21.
//

import UIKit

class ReportVC: UIViewController {

    @IBOutlet weak var commentView: UIView!
    @IBOutlet weak var viewHeightCons: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()

        commentView.isHidden = true
        viewHeightCons.constant = 580
        let gestureRecognizer = UIPanGestureRecognizer(target: self,
                                                        action: #selector(panGestureRecognizerHandler(_:)))
         view.addGestureRecognizer(gestureRecognizer)
    }
    
    @IBAction func dropDownBtnTapped(_ sender: UIButton) {
        commentView.isHidden = false
        viewHeightCons.constant = 700
    }
    @IBAction func panGestureRecognizerHandler(_ sender: UIPanGestureRecognizer) {
        let touchPoint = sender.location(in: view?.window)
        var initialTouchPoint = CGPoint.zero

        switch sender.state {
        case .began:
            initialTouchPoint = touchPoint
        case .changed:
            if touchPoint.y > initialTouchPoint.y {
                view.frame.origin.y = touchPoint.y - initialTouchPoint.y
            }
        case .ended, .cancelled:
            if touchPoint.y - initialTouchPoint.y > 200 {
                dismiss(animated: true, completion: nil)
            } else {
                UIView.animate(withDuration: 0.2, animations: {
                    self.view.frame = CGRect(x: 0,
                                             y: 0,
                                             width: self.view.frame.size.width,
                                             height: self.view.frame.size.height)
                })
            }
        case .failed, .possible:
            break
        }
    }
    @IBAction func dismisssViewBtnTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
