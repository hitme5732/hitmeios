//
//  ProfileVC.swift
//  HITME
//
//  Created by SAI KUMAR on 29/09/21.
//

import UIKit

class ProfileVC: UIViewController {

    var content = ["Update Profile","Change Password","Select Content Language","Next Target Level","Settings","Permission","Logout"]
    override func viewDidLoad() {
        super.viewDidLoad()
        let gestureRecognizer = UIPanGestureRecognizer(target: self,
                                                        action: #selector(panGestureRecognizerHandler(_:)))
         view.addGestureRecognizer(gestureRecognizer)
        // Do any additional setup after loading the view.
    }
    @IBAction func panGestureRecognizerHandler(_ sender: UIPanGestureRecognizer) {
        let touchPoint = sender.location(in: view?.window)
        var initialTouchPoint = CGPoint.zero

        switch sender.state {
        case .began:
            initialTouchPoint = touchPoint
        case .changed:
            if touchPoint.y > initialTouchPoint.y {
                view.frame.origin.y = touchPoint.y - initialTouchPoint.y
            }
        case .ended, .cancelled:
            if touchPoint.y - initialTouchPoint.y > 200 {
                dismiss(animated: true, completion: nil)
            } else {
                UIView.animate(withDuration: 0.2, animations: {
                    self.view.frame = CGRect(x: 0,
                                             y: 0,
                                             width: self.view.frame.size.width,
                                             height: self.view.frame.size.height)
                })
            }
        case .failed, .possible:
            break
        }
    }

    @IBAction func dismissAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
extension ProfileVC:UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0{
            return 1
        }else{
            return content.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileHeaderTVC") as? ProfileHeaderTVC else {
                return UITableViewCell()
            }
            return cell
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileSettingTVC") as? ProfileSettingTVC else {
                return UITableViewCell()
            }
            cell.contentLbl.text = content[indexPath.row]
            return cell
        default:
            break
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.section {
        case 0:
            return 140
        case 1:
            return 55
        default:
            break
        }
       return 0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 5 {
            self.dismiss(animated: true) {
                let storyBoard = UIStoryboard(name:"Main", bundle: nil)
                if let conVC = storyBoard.instantiateViewController(withIdentifier: "VideoPermissionsVC") as? VideoPermissionsVC,
                    let navController = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
                    navController.present(conVC, animated: true, completion: nil)
                }
            }
           
        }else if  indexPath.row == 1 {
            self.dismiss(animated: true) {
                let storyBoard = UIStoryboard(name:"Main", bundle: nil)
                if let conVC = storyBoard.instantiateViewController(withIdentifier: "MobileupdatePasswordVC") as? MobileupdatePasswordVC,
                    let navController = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
                    navController.pushViewController(conVC, animated: false)
                }
            }
        }else if indexPath.row == 3 {
            self.dismiss(animated: true) {
                let storyBoard = UIStoryboard(name:"Main", bundle: nil)
                if let conVC = storyBoard.instantiateViewController(withIdentifier: "NextLevelVC") as? NextLevelVC,
                    let navController = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
                    navController.present(conVC, animated: true, completion: nil)
                }
            }
        }else if indexPath.row == 2 {
            self.dismiss(animated: true) {
                let storyBoard = UIStoryboard(name:"Main", bundle: nil)
                if let conVC = storyBoard.instantiateViewController(withIdentifier: "LanguageSelectionVC") as? LanguageSelectionVC,
                    let navController = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
                    navController.present(conVC, animated: true, completion: nil)
                }
            }
        }
    }
    
}
