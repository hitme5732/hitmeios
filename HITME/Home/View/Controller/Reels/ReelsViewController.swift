//
//  ReelsViewController.swift
//  HITME
//
//  Created by Rahul Sharma on 29/09/21.
//

import UIKit
import AVKit
import AVFoundation

class ReelsViewController: UIViewController {
    var avpController = AVPlayerViewController()
    @IBOutlet weak var reelsCollectionView:UICollectionView!
//    var player = AVPlayer()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        reelsCollectionView.register(ReelsCVC.nib, forCellWithReuseIdentifier: ReelsCVC.reuseIdentifier)
        reelsCollectionView.isPagingEnabled = true
    }

}

extension ReelsViewController: UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ReelsCVC.reuseIdentifier, for: indexPath) as? ReelsCVC else{return UICollectionViewCell()}
        
        if indexPath.row == 0 {
            guard let path = Bundle.main.path(forResource: "WhatsApp Video 2021-03-19 at 12.26.02 PM", ofType:"mp4") else {
                fatalError()
            }
            cell.player = AVPlayer(url: URL(fileURLWithPath: path))
            let playerLayer = AVPlayerLayer(player: cell.player)
            playerLayer.frame = self.view.bounds
            avpController.view.frame.size.height = self.view.frame.size.height
            avpController.view.frame.size.width = self.view.frame.size.width
            cell.playerView.layer.addSublayer(playerLayer)
//            cell.player?.play()
        }else if indexPath.row == 1 {
            guard let path = Bundle.main.path(forResource: "WhatsApp Video 2021-03-19 at 12.29.12 PM", ofType:"mp4") else {
                fatalError()
            }
            cell.player = AVPlayer(url: URL(fileURLWithPath: path))
            let playerLayer = AVPlayerLayer(player: cell.player)
            playerLayer.frame = self.view.bounds
            avpController.view.frame.size.height = self.view.frame.size.height
            avpController.view.frame.size.width = self.view.frame.size.width
            cell.playerView.layer.addSublayer(playerLayer)
//            cell.player?.play()
        }else if indexPath.row == 2 {
            guard let path = Bundle.main.path(forResource: "WhatsApp Video 2021-03-26 at 11.43.55 AM", ofType:"mp4") else {
                fatalError()
            }
            cell.player = AVPlayer(url: URL(fileURLWithPath: path))
            let playerLayer = AVPlayerLayer(player: cell.player)
            playerLayer.frame = self.view.bounds
            avpController.view.frame.size.height = self.view.frame.size.height
            avpController.view.frame.size.width = self.view.frame.size.width
            cell.playerView.layer.addSublayer(playerLayer)
//            cell.player?.play()
        }
      
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.width, height: self.view.frame.height)
    }
}
extension ReelsViewController: UIScrollViewDelegate {
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
       /* reelsCollectionView.visibleCells.forEach { cell in
            guard let cell = cell as? ReelsCVC else {return}
            cell.player?.pause()
        }*/
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        /*reelsCollectionView.visibleCells.forEach { cell in
            // TODO: write logic to start the video after it ends scrolling
            guard let cell = cell as? ReelsCVC else {return}
            cell.player?.play()
        }*/
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let visibleCells = self.reelsCollectionView.indexPathsForVisibleItems
            .sorted { top, bottom -> Bool in
                return top.section < bottom.section || top.row < bottom.row
            }.compactMap { indexPath -> UICollectionViewCell? in
                return self.reelsCollectionView.cellForItem(at: indexPath)
            }
        let indexPaths = self.reelsCollectionView.indexPathsForVisibleItems.sorted()
        let cellCount = visibleCells.count
        guard let firstCell = visibleCells.first as? ReelsCVC, let firstIndex = indexPaths.first else {return}
        checkVisibilityOfCell(cell: firstCell, indexPath: firstIndex)
        if cellCount == 1 {return}
        guard let lastCell = visibleCells.last as? ReelsCVC, let lastIndex = indexPaths.last else {return}
        checkVisibilityOfCell(cell: lastCell, indexPath: lastIndex)
    }
    func checkVisibilityOfCell(cell: ReelsCVC, indexPath: IndexPath) {
        if let cellRect = (reelsCollectionView.layoutAttributesForItem(at: indexPath)?.frame) {
            let completelyVisible = reelsCollectionView.bounds.contains(cellRect)
            if completelyVisible {cell.player?.play()} else {cell.player?.pause()}
        }
    }
}
