//
//  ReelsContainerVC.swift
//  HITME
//
//  Created by Rahul Sharma on 29/09/21.
//

import UIKit

class ReelsContainerVC: UIViewController {

    
    @IBOutlet weak var fellowName: UILabel!
    @IBOutlet weak var fellowDescription: UILabel!
    
    @IBOutlet weak var followTitle: UILabel!
    @IBOutlet weak var reelsContainer: UIView!
    @IBOutlet weak var customTabBarContainer: UIView!
    
    @IBOutlet weak var drawerTopHight: NSLayoutConstraint! // 18
    
    @IBOutlet weak var drawerBottom: NSLayoutConstraint! //0
    @IBOutlet weak var profileReelsCollectionView:UICollectionView!
    @IBOutlet weak var hitsTitle:UILabel!
    @IBOutlet weak var commentTitle:UILabel!
    @IBOutlet weak var shareTitle:UILabel!
    @IBOutlet weak var reportTitle:UILabel!
    @IBOutlet weak var profileTitle:UILabel!
    
    @IBOutlet weak var bottomContainer:UIView!
    
    @IBOutlet weak var bottomContainerStack: UIStackView!
    
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var drawerImageView: UIImageView!
    @IBOutlet weak var drawerButton: UIButton!
    
    var hideBottomDrawer:Bool = true{
        didSet{
            bottomView.isHidden = hideBottomDrawer
            bottomContainerStack.isHidden = hideBottomDrawer
            bottomContainer.isHidden = hideBottomDrawer
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideBottomDrawer = true
        drawerImageView.transform = hideBottomDrawer ? .identity : CGAffineTransform(rotationAngle: .pi)
        addReels()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    
    @IBAction func bottomDrawer(_ sender: Any) {
        UIView.animate(withDuration: 0.25) {
            self.hideBottomDrawer = !self.hideBottomDrawer
            self.drawerBottom.constant = self.hideBottomDrawer ? 0 : -30
            self.drawerImageView.transform = self.hideBottomDrawer ? .identity : CGAffineTransform(rotationAngle: .pi)
            self.view.layoutIfNeeded()
        }
        DispatchQueue.main.async {
            self.profileReelsCollectionView.reloadData()
        }
    }
    
    @IBAction func searchAction(_ sender: Any) {
       let reel =  SearchVC.instantiate(storyBoardName: "Main")
        self.navigationController?.present(reel, animated: true, completion: nil)
    }
    
    @IBAction func followingAction(_ sender: Any) {
        
        let vc = FollowingVC.instantiate(storyBoardName: "Main")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func reports(_ sender: Any) {
        let vc = ReportVC.instantiate(storyBoardName: "Main")
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    @IBAction func comments(_ sender: Any) {
        let vc = CommentsVC.instantiate(storyBoardName: "Main")
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func profile(_ sender: Any) {
        let vc = ShareProfileVC.instantiate(storyBoardName: "Main")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func hits(_ sender: Any){
        
    }
    
    @IBAction func startReelAction(_ sender: Any) {
        
        let vc = LiveCameraVC.instantiate(storyBoardName: "Main")
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func share(_ sender: Any){
        let text = "This is the text....."
          let textShare = [ text ]
          let activityViewController = UIActivityViewController(activityItems: textShare , applicationActivities: nil)
          activityViewController.popoverPresentationController?.sourceView = self.view
          self.present(activityViewController, animated: true, completion: nil)
    }
    
    func addReels(){
        let reelsVC = ReelsViewController.instantiate(storyBoardName: "Home")
        addChild(reelsVC)
        reelsContainer.addSubview(reelsVC.view)
        reelsVC.didMove(toParent: self)
        reelsVC.view.frame = reelsContainer.bounds
    }

}

extension ReelsContainerVC: UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return hideBottomDrawer ? 0 : 8
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProfileReelCVC.reuseIdentifier, for: indexPath) as? ProfileReelCVC else{return UICollectionViewCell()}
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.height - 6, height: collectionView.frame.height - 6)
    }
    
}
