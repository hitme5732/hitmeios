//
//  ReelsCVC.swift
//  HITME
//
//  Created by Rahul Sharma on 29/09/21.
//

import UIKit
import AVFoundation

class ReelsCVC: UICollectionViewCell {
    
    @IBOutlet weak var playerView: UIView!
    @IBOutlet weak var playerContainer: UIView!
    var player:AVPlayer?
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        player = nil
    }
}
