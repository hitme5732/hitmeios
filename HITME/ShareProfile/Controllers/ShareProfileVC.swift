//
//  ShareProfileVC.swift
//  HITME
//
//  Created by Naresh Oruganti on 05/10/21.
//

import UIKit

class ShareProfileVC: UIViewController {
    @IBOutlet weak var bottomCollectionVC: UICollectionView!
    var arrayOfLabel = ["Post Video","Saved Video","Photos"]
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    @IBAction func settingOptionAction(_ sender: Any) {
        let vc = ProfileVC.instantiate(storyBoardName: "Main")
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func shareProfile(_ sender: Any) {
        let text = "This is the text....."
          let textShare = [ text ]
          let activityViewController = UIActivityViewController(activityItems: textShare , applicationActivities: nil)
          activityViewController.popoverPresentationController?.sourceView = self.view
          self.present(activityViewController, animated: true, completion: nil)
    }
    @IBAction func searchAction(_ sender: Any) {
        let reel =  SearchVC.instantiate(storyBoardName: "Main")
         self.navigationController?.present(reel, animated: true, completion: nil)
    }
    @IBAction func goToHomeAction(_ sender: Any) {
        let home = ReelsContainerVC.instantiate(storyBoardName: "Home")
        self.navigationController?.pushViewController(home, animated: true)
    }
}


extension ShareProfileVC:UICollectionViewDelegate,UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == bottomCollectionVC {
            return 6
        }else {
            return 3
        }
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == bottomCollectionVC {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BottomCVC", for: indexPath) as? BottomCVC else {
                fatalError("Unexpected indexPath")
            }
            return cell
        }else {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProfileHeaderCVC", for: indexPath) as? ProfileHeaderCVC else {
                fatalError("Unexpected indexPath")
            }
            cell.topLabel.text = arrayOfLabel[indexPath.item]
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == bottomCollectionVC {
            if indexPath.item == 0 {
                let home = VideoPlayVC.instantiate(storyBoardName: "Main")
                self.navigationController?.pushViewController(home, animated: true)
            }else {
                let home = CashOutImagesVC.instantiate(storyBoardName: "Main")
                self.navigationController?.pushViewController(home, animated: true)
            }
        }
    }
}


extension ShareProfileVC:UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout:
                            UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == bottomCollectionVC {
            let picDimension = (collectionView.frame.width/3)
            return CGSize(width:(picDimension), height: 100)
        }else {
            let picDimension = (collectionView.frame.width/3)
            return CGSize(width:(picDimension), height: collectionView.frame.height)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
