//
//  NextLevelVC.swift
//  ProfileScreens
//
//  Created by Naresh Oruganti on 04/10/21.
//

import UIKit

class NextLevelVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
}


extension NextLevelVC:UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 3
        }else {
            return 1
        }
       
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "FollowersTVC", for: indexPath) as? FollowersTVC else {
                fatalError("Unexpected indexPath")
            }
            return cell
        }else  {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "YourLevelTVC", for: indexPath) as? YourLevelTVC else {
                fatalError("Unexpected indexPath")
            }
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 75
        }else {
            return 90
        }
    }
}
